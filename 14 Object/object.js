var mahasiswa = {
    nama : 'robi kurniawan',
    lulus : true,
    IPSemester: [2.8, 3.5, 3.3, 4.0, 3.0],
    IPKumulatif: function(){
        var total = 0;
        var ips = this.IPSemester;
        for (var i=0; i < ips.length; i++){
            total += ips[i];
        }
        return total/ips.length;
    }
}

console.log(mahasiswa.IPKumulatif());