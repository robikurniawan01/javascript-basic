//Object Literal
var mhs1 = {
    nama: "robi kurniawan",
    nim: '312016067',
    email: 'robikurniawan01@gmail,com',
    jurusan: 'Teknik Komputer'
}

var mhs2 = {
    nama: "dodi",
    nim: '312016068',
    email: 'dodikurniawan01@gmail,com',
    jurusan: 'Teknik Komputer'
}

//Function declaration
function buatObjectMahasiswa (nama, nim, email, jurusan) {
    var mhs = {};
    mhs.nama = nama;
    mhs.nim = nim;
    mhs.email = email;
    mhs.jurusan = jurusan;
    return mhs;
}

var mhs3 = buatObjectMahasiswa ('nofariza', '3120173', 'noza@mail.com', 'elektro');
console.log(mhs3);

//Constructor
function Mahasiswa (nama, nim , email, jurusan) {
    this.nama = nama;
    this.nim = nim;
    this.email = email;
    this.jurusan = jurusan;
}

var mhs4 = new Mahasiswa('robi', '3120173', 'robi@mail.com', 'teknik komputer');
console.log(mhs4);