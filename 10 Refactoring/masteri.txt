Refactoring adalah sebuah proses untuk mengubah 
kode kita menjadi lebih baik tanpa mengubah fungsionalitasnya

Why Refactoring ?
1. Readability
2. Dont repeat your self
3. Testability
4. Performance
5. Maintainability

//Sebelum Refactoring

function jumlah2buahKubus (a, b) {
    var volumeA, volumeB, total;
    
    volumeA = a * a * a;
    volumeB = b * b * b;
    total = volumeA + volumeB;
    return total;
}
console.log(jumlah2buahKubus(8, 3));

//Sesudah Refactoring

function ref (a, b) {
    return a*a*a + b*b*b;
    }

console.log(ref(8, 3));
