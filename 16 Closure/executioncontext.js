console.log(nama); //undefined
var nama = 'Robi kurniawan';

//1. creation phase pada Global context
// nama var = undefined
// nama function = fn()
//hoisting
//window sebagai global object
// this = window

//2. Execution phase

var nama = 'robi kurniawan';
var umur = 23;


function sayHello (){
    console.log(`Halo , nama saya ${nama} , saya ${umur} tahun`);
}
console.log(sayHello())
    
//2. Function membuat local execution context
//yang didalamnya terdapat creation phase dan execution phase
//bisa akses windows 
// nama var = undefined
// nama fun = fn()
// bisa akses argument 
//hoisting

var name = 'Robi kurniawan';
var username = '@robikurniawans';
function cetakURL(username){
    var instagramurl = 'http://www.instagram.com/';
    return instagramurl+username;
}
console.log(cetakURL(username));