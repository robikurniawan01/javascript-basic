// let add = function () {
//     let counter = 0;
//     return function () {
//         return ++counter;
//     }
// }

// let a = add();

// console.log(a());
// console.log(a());
// console.log(a());

//Immediately invoked function
let add = (function () {
    let counter = 0;
    return function () {
        return ++counter;
    }
})();


console.log(add());
console.log(add());
console.log(add());