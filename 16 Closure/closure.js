function ucapkanSalam (waktu) {
    return function(nama){
        console.log(`halo ${nama}, selamat ${waktu} semoga harimu menyenangkan`);
    }
}

let selamatPagi = ucapkanSalam('pagi');
let selamatSiang = ucapkanSalam('siang');
let selamatMalam = ucapkanSalam('Malam'); 

selamatPagi('Robi kurniawan');