//Cara membuat object
//1. Object Literal
let mahasiswa = {
    nama : 'Robi kurniawan',
    energy : 10,
    makan: function(porsi){
        this.energy = this.energy + porsi;
        console.log(`Selamat datang ${this.nama}, selamat makan`);
    }
}

//2. Function Declaration
function Mahasiswa (nama, energy) {
    let mahasiswa = {};
    mahasiswa.nama = nama;
    mahasiswa.energy = energy;
    mahasiswa.makan = function(porsi) {
        this.energy = this.energy + porsi;
        console.log(`Selamat datang ${this.nama}, selamat makan`);
    }
    mahasiswa.main = function(jam) {
        this.energy -= jam;
        console.log(`Halo ${this.nama}, selamat bermain`);
    }
    return mahasiswa;
} 

let Robi = Mahasiswa('robi', 20);

// 3.Constructor Function
function Murid (nama, energy) {
    this.nama = nama;
    this.energy = energy;
    this.makan = function(porsi) {
        this.energy = this.energy + porsi;
        console.log(`Selamat datang ${this.nama}, selamat makan`);
    }
    this.main = function(jam) {
        this.energy -= jam;
        console.log(`Halo ${this.nama}, selamat bermain`);
    }
}
let Dody = new Murid ('Dody', 30);

// 4. Object Create 
let MethodAnak = {
    makan : function(porsi) {
        this.energy = this.energy + porsi;
        console.log(`Selamat datang ${this.nama}, selamat makan`);
    },
    main : function(jam) {
        this.energy -= jam;
        console.log(`Halo ${this.nama}, selamat bermain`);
    },
    tidur : function(jam){
        this.energy += jam * 2;
        console.log(`Halo ${this.nama}, selamat tidur`);
    }
}

function Anak (nama, energy) {
    let anak = Object.create(MethodAnak);
    anak.nama = nama;
    anak.energy = energy;

    return anak;
}

let Yudi = Anak('yudi', 10);
