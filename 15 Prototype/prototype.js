//Versi Prototype
// function Mahasiswa (nama, energy){
//     this.nama = nama;
//     this.energy = energy;
// }
// Mahasiswa.prototype.makan = function(porsi){
//     this.energy += porsi;
//     return `Haalo ${this.nama}, Selamat makan`
// }
// Mahasiswa.prototype.main = function(jam) {
//     this.energy -= jam;
//     console.log(`Halo ${this.nama}, selamat bermain`);
// },
// Mahasiswa.prototype.tidur = function(jam){
//     this.energy += jam * 2;
//     console.log(`Halo ${this.nama}, selamat tidur`);
// }

// let robi = new Mahasiswa('robi', 10)


//Versi Kelas
class Mahasiswa {
    constructor(nama, energy){
        this.nama = nama;
        this.energy = energy;
    }
    makan(porsi){
    this.energy += porsi;
    return `Haalo ${this.nama}, Selamat makan`
    }

    main (jam) {
        this.energy -= jam;
        console.log(`Halo ${this.nama}, selamat bermain`);
    }

    tidur (jam){
        this.energy += jam * 2;
        console.log(`Halo ${this.nama}, selamat tidur`);
    }
}

let robi = new Mahasiswa ('robi', 10);