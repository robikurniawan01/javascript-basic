//Manipulasi array

//1.menambah isi array
var arr = ['a', 1, true];
console.log(arr[1]);

var arr1 = [];
arr1[0] = "robi";
arr1[1] = "kurniawan";
arr1[2] = true;

//2. Menghapus isi array
var arr2 = ['a', 1, true];
arr2 = undefined;

//3. menampilkan seluruh isi array
var nama = ['robi', 'sandika', 'galih'];
for (var i = 0; i < nama.length; i++){
    console.log('mahasiswa ke' + (i+1) + ' : ' + nama[i]);
}

//array method
//1. length
nama.length;
//2. join : menggabungkan seluruh isi array dan jadi string
console.log(nama.join(' - '));
//3. push / menmabah array di akhir array
nama.push('doddy', 'rijal'); //tambah doddy
//4. Menghilangkan element terakhir di array
nama.pop(); //rijal hilang
//5. unshift / menambah array di awal array
nama.unshift('restu') // tambah restu di awal
//6. shift / hapus isi array di awal
nama.shift(); // restu hilang
//7. slice / mengiris array mengambil beberapa bagian array dan masukan ke var baru
//NamaArray.slice(awal, akhir)
var sukabumi = ['kalapanunggal', 'kabandungan', 'cisaat' , 'jampang'];
var newArray = sukabumi.slice(1, 3); // newArray = ['kabandungan', 'cisaat];
//8. Splice / menyisipkan sebuah elemnt ke tengah array
// namaarray.splice(indexAwal, maudihapusBerapa, elemenBaru1, elementBaru2);
var hobi = ['mancing', 'berenang', 'ngoding'];
hobi.splice(1, 0, 'lari'); //menambhkan lari setelah berenang tanpa hapus
console.log(hobi);

//9. forEach
var angka = [1,2,3,4,5,6,7,8,9];
angka.forEach(function(e){
    console.log(e);
})
var mahasiswa = ['robi', 'sahrul', 'nirfan', 'wildan'];
mahasiswa.forEach(function(e, i){
    console.log('mahasiswa ke ' + (i+1) + ' adalah ' + e);
})

//10. map hampir sama dengan forEach namun bisa mengmbalikan array
var angka2 = [1,2,3,4,5,6,7,8,9];
var angka3 = angka2.map(function(e){
    return e * 2;
})
console.log(angka3.join(' - '));

//11. sort
var angka4 = [5,545,5,3,355,4,34,5];
angka4.sort(function(a, b){
    return a - b;
});
console.log(angka4.join(' - '));

//12. filter mencari nilai dan bisa mengembalikan lebih dari 1 nilai
var angka5 = angka4.filter(function(e){
    return e > 354;
});
console.log(angka5.join(' - '));

// 13 find / menemukan satu nilai saja
var angka6 = angka4.find(function(x){
    return x == 4;
})
console.log(angka6);