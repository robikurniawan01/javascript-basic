//Function expression

// let tampilkanNama = (nama) => { return `halo ${nama}`;}
// console.log(tampilkanNama('robi kurniawan'))

//hanya 1 parameter
// let tampilkanNama = nama =>  `halo ${nama}`;
// console.log(tampilkanNama('robi kurniawan'))

//tanpa parameter
// const tampilkanNama = () => `hello world`;
// console.log(tampilkanNama());


//==============================================

// let Mahasiswa = ['sandila', 'robi', 'indri'];
// let jumlahHuruf = Mahasiswa.map(function(nama){
//     return nama.length;
// });
// console.log(jumlahHuruf)

// let Mahasiswa = ['sandika', 'robi', 'indri'];
// let jumlahHuruf = Mahasiswa.map( nama => nama.length);
// console.table(jumlahHuruf)


//Mengembalikan sebagai object

let Mahasiswa = ['sandika', 'robi', 'indri'];
let jumlahHuruf = Mahasiswa.map( nama => ({nama: nama, jmlhHuruf: nama.length}));
console.table(jumlahHuruf)



//==============================================