//contoh penerapan :
// 1. angka tanpa desimal akurat sampai 15 digit
var number1 = 12;

// 2. angka dengan desimal bisa sampai 17 digit dibelakang koma
var number2 = 3.14;

// 3. eksponen
var eksponen = 123e5;
//return 123500000
var eksponenmin = 123e-5;
//return 0.00123
// 4. bilangan negatif
var neg = -10;


//Angka special
var infinity = 2/0;
var infinityNegatif = -2/0;

//NaN Not a Number
var nan1 = 5 / "robi";
var Nan2 = 0/0;

//Note : jangan masukan angka 0 didepan bilangan
