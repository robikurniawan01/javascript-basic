//merepresentasikan data textual
var str1 = "Hari jum'at";
var str2 = 'mari kita "Jaga bersihan"';
//Escape character \' dll
var str3 = '"Gerakan buang sampah" dilaksanakan hari jum\'at';

//Concatenation str1+str2
//membandingkan 2 string str1  == str2 //Case sensitif
// fungsi string ex:  str.length

console.log(str3);